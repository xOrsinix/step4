// Return the set of products that were ordered by all customers
fun Shop.getProductsOrderedByAll(): Set<Product> {
    val prod=customers.flatMap{it.getOrderedProducts()}.toSet()
    return customers.fold(prod){
            a,b->a.intersect(b.getOrderedProducts())
    }
}

fun Customer.getOrderedProducts(): List<Product> = orders.flatMap{it.products}